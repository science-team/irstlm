irstlm (6.00.05-5) unstable; urgency=medium

  * Team upload.
  * Drop dh-buildinfo from Build-Depends (see bug #1068809)
  * No need to mention libtool, automake and autoconf explicitly in
    Build-Depends
  * Standards-Version: 4.7.0 (routine-update)
  * Remove patches online_documentation_enhancments.patch,
    spelling_fixes.patch that are missing from debian/patches/series.

 -- Andreas Tille <tille@debian.org>  Wed, 27 Nov 2024 12:22:11 +0100

irstlm (6.00.05-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062458

 -- Lukas Märdian <slyon@debian.org>  Wed, 28 Feb 2024 11:48:59 +0000

irstlm (6.00.05-4) unstable; urgency=medium

  * Remove MIA uploader Koichi Akabe <vbkaisetsu@gmail.com>. Closes: #864577

 -- Bastian Germann <bage@debian.org>  Tue, 19 Sep 2023 22:53:34 +0200

irstlm (6.00.05-3) unstable; urgency=medium

  * Team upload.
  * Fix VCS fields
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * No tab in license text (routine-update)
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Apply multi-arch hints.
    + libirstlm-dev: Add Multi-Arch: same.
  * watch file standard 4 (routine-update)
  * Enable dh_missing --fail-missing

 -- Andreas Tille <tille@debian.org>  Mon, 15 Nov 2021 20:15:00 +0100

irstlm (6.00.05-2) unstable; urgency=medium

  * Add missing LPPL license to copyright.
    Closes: #824455
  * Add path command to irstlm wrapper script.
  * Change maintainer to Debian Science Maintainers
    <debian-science-maintainers@lists.alioth.debian.org>
  * Updated Vcs-* fields in control.
    The repository has been moved to debian-science.

 -- Giulio Paci <giuliopaci@gmail.com>  Tue, 17 May 2016 09:45:14 +0200

irstlm (6.00.05-1) unstable; urgency=medium

  * New upstream version 6.00.05.
  * Update watch file.
    Project has migrated to github.
  * Bump Standards-Version to 3.9.8.
    No changes needed.
  * Update Vcs-* fields in control.
    Secure urls are now used.
  * Update Homepage field in control.
    Project has an homepage at FBK site.
  * Change Section from text to science.
  * Update Source field in copyright.
    Project has migrated to github.
  * Drop 1001_fix_compilation_warnings.patch and
    compilation_fixes.patch.
  * Add 1001_fix_build_issues.patch, 1003_fix_online_help.patch
    and 1004_do_not_install_private_scripts.patch.
  * Update 1002_fix_problems_identified_by_cppcheck.patch.
  * Drop irstlm.lintian-overrides.
  * Rename libirstlm0 package into libirstlm1.
  * Remove --includedir configure flag.
    This option is not needed with provided patches.
  * Update bash completion file.
  * Handle bash completion installation with irstlm.bash-completion.
    - Add bash-completion to Build-Depends.
    - Add irstlm.bash-completion.
    - Update irstlm.install.
    - Use --with bash-completion dh option.
  * Implement a workaround to handle plsa and plsa.sh in irstlm and
    update_completion.

 -- Giulio Paci <giuliopaci@gmail.com>  Fri, 06 May 2016 01:40:07 +0200

irstlm (5.80.03-2) unstable; urgency=medium

  * Install headers in /usr/include/irstlm.
    Closes: #812742, #813093.

 -- Giulio Paci <giuliopaci@gmail.com>  Tue, 26 Jan 2016 14:31:34 +0100

irstlm (5.80.03-1) unstable; urgency=medium

  [Giulio Paci]
  * Initial release.
    Closes: #671282.

  [Balint Reczey]
  * Build-depend on default automake
  * Convert packaging to use Debhelper
  * Add myself to uploaders
  * Convert to multiarch
  * Drop debug package
  * Drop control.in
  * Install bash-completion rules to proper location
  * Update standards version
  * Drop unused copyright info

 -- Balint Reczey <balint@balintreczey.hu>  Sun, 20 Dec 2015 14:09:20 +0100
